function NhanVien(
  taikhoan,
  hoten,
  email,
  matkhau,
  ngaylam,
  luongcoban,
  chucvu,
  giolam,
  tongluong,
  loai
) {
  this.taikhoan = taikhoan;
  this.hoten = hoten;
  this.email = email;
  this.matkhau = matkhau;
  this.ngaylam = ngaylam;
  this.luongcoban = luongcoban;
  this.chucvu = chucvu;
  this.giolam = giolam;
  this.tongluong = tongluong;
  this.loai = loai;
}
