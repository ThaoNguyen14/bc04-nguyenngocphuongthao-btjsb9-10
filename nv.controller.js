function layThongTinTuForm() {
  const taikhoan = document.getElementById("tknv").value;
  const hoten = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const matkhau = document.getElementById("password").value;
  const ngaylam = document.getElementById("datepicker").value;
  const luongcoban = document.getElementById("luongCB").value;
  const chucvu = document.getElementById("chucvu").value;
  const giolam = document.getElementById("gioLam").value;

  return new NhanVien(
    taikhoan,
    hoten,
    email,
    matkhau,
    ngaylam,
    luongcoban,
    chucvu,
    giolam
  );
}

// render array NV ra giao dien
function renderDSNV(nvArr) {
  console.log("nvArr: ", nvArr);
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    //trContent thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
    <td>${nv.taikhoan}</td>
    <td>${nv.hoten}</td>
    <td>${nv.email}</td>
    <td>${nv.ngaylam}</td>
    <td>${nv.chucvu}</td>
    <td>${tinhTongLuong(nv.chucvu, nv.luongcoban)}</td>
    <td>${xeploai(nv.giolam)}</td>
    <td>
    <btn onclick="xoaNhanVien(${i})" class = "btn btn-danger">Xóa</btn>
    <btn onclick="suaNhanVien(${i})"data-toggle="modal" data-target="#myModal" class = "btn btn-warning">Sửa</btn>
    
    </td>

    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
  // tbodySinhVien;
}
// Lay danh sach nhan vien localstorage
function getDSNV() {
  return JSON.parse(window.localStorage.getItem("DSNV"));
}

// luu danh sach nhan vien vao localstorage
function saveDSNV() {
  // tạo json
  var dsnvJson = JSON.stringify(DSNV);
  // luu json vào localstorage
  localStorage.setItem("DSNV", dsnvJson);
}

function tinhTongLuong(chucvu, luongcoban) {
  let tongluong = 0;
  switch (chucvu) {
    case "Sếp":
      tongluong = luongcoban * 3;
      break;
    case "Trưởng phòng":
      tongluong = luongcoban * 2;
      break;
    default:
      tongluong = luongcoban;
      break;
  }
  return tongluong;
}

function xeploai(giolam) {
  if (giolam >= 192) {
    return "nhân viên xuất sắc";
  }
  if (giolam >= 176) {
    return "nhân viên giỏi";
  }
  if (giolam >= 160) {
    return "nhân viên khá";
  }
  return "nhân viên trung bình";
}

function xoaNhanVien(index) {
  DSNV.splice(index, 1);
  renderDSNV(DSNV);
  saveDSNV();
}

function suaNhanVien(index) {
  // Tai du lieu nhan vien len form
  loadNhanVien(DSNV[index]);

  ViTriNhanVienDangSua = index;
}

function capNhatNhanVien() {
  DSNV[ViTriNhanVienDangSua] = layThongTinTuForm();

  renderDSNV(DSNV);

  saveDSNV();

  ViTriNhanVienDangSua = null;
}
// Tai du lieu nhan vien len form

function loadNhanVien(NhanVien) {
  if (NhanVien) {
    document.getElementById("tknv").value = NhanVien.taikhoan;
    document.getElementById("name").value = NhanVien.hoten;
    document.getElementById("email").value = NhanVien.email;
    document.getElementById("password").value = NhanVien.matkhau;
    document.getElementById("datepicker").value = NhanVien.ngaylam;
    document.getElementById("luongCB").value = NhanVien.luongcoban;
    document.getElementById("chucvu").value = NhanVien.chucvu;
    document.getElementById("gioLam").value = NhanVien.giolam;
  }
}

function timNhanVien() {
  // Lay tu khoa
  const str = document.getElementById("searchName").value;

  const result = DSNV.filter((nv) => xeploai(nv.giolam).includes(str));

  renderDSNV(result);
}
