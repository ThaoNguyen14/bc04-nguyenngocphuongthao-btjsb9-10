let DSNV = getDSNV() || []; // -> Lay dsnv tu localStorage; nue dsnv = null thi do danh sach nhan vien = []
let ViTriNhanVienDangSua = null;
renderDSNV(DSNV);
console.log(DSNV);
function themNV() {
  var newNV = layThongTinTuForm();

  // kiem tra ma NV
  var isValid =
    validator.kiemTraRong(
      newNV.taikhoan,
      "tbTKNV",
      "Tài khoản NV không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newNV.taikhoan,
      "tbTKNV",
      "Tài khoản NV phải gồm 4-6 ký tự",
      4,
      6
    );
  //kiem tra ten NV
  isValid =
    isValid &
    validator.kiemTraRong(newNV.hoten, "tbTen", "Tên NV không được để trống");
  //kiem tra email SV
  isValid =
    isValid &
    validator.kiemTraRong(
      newNV.email,
      "tbEmail",
      "Email NV không được để trống"
    );

  //kiem tra password ko duoc rong
  isValid =
    isValid &
    isValid &
    validator.kiemTraRong(
      newNV.matkhau,
      "tbMatKhau",
      "Mật khẩu NV không được để trống"
    );

  // kiemtraluongcoban 1000000-20000000
  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemtraluongcoban(
      newNV.luongcoban,
      "tbLuongCB",
      "Lương cơ bản từ 1,000,000 đến 20,000,000",
      1000000,
      20000000
    );

  //kiem ngay lam khong duoc de trong va dinh dang dd/mm/yyyy
  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemTraRong(
      newNV.chucvu,
      "tbChucVu",
      "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
    );
  console.log(
    validator.kiemTraRong(
      newNV.chucvu,
      "tbChucVu",
      "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
    ),
    newNV.chucvu
  );

  //kiem tra chon chuc vu hop le

  //kiem tra so gio lam ko duoc de trong và tu 80-200h
  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemtrasogiolam(
      newNV.giolam,
      "tbGiolam",
      "Số giờ làm từ 80 đến 200 giờ",
      80,
      200
    );

  isValid =
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    isValid &
    validator.kiemtrasogiolam(
      newNV.giolam,
      "tbGiolam",
      "Số giờ làm từ 80 đến 200 giờ",
      80,
      200
    );

  // nếu hợp lệ mới thêm
  if (isValid) {
    DSNV.push(newNV);

    // re render html voi dsnv moi
    renderDSNV(DSNV);

    saveDSNV(DSNV);

    //console.log("dssv: ", dssv);
  }
}
