var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return true;
    } else {
      document.getElementById(idError).innerText = "";
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";

      return false;
    } else {
      document.getElementById(idError).innerText = "";

      return true;
    }
  },

  // regex email js
  kiemtraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";

      return false;
    }
  },

  kiemtraluongcoban: function (value, idError, message, min, max) {
    if (value < min || value > max) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";

      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemtrasogiolam: function (value, idError, message, min, max) {
    if (value < min || value > max) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";

      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
};
